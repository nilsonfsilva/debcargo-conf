Description: Serialize tests with race conditions
 breezyshim's testing::TestEnv unconditionally accesses and modifies the
 testing environment. This causes a race condition when tests that use it
 are executed in parallel, making them fail sometimes. Fix: execute such
 tests in series using serial_test.
Author: NoisyCoil <noisycoil@tutanota.com>
Last-Update: 2024-10-23
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -232,6 +232,9 @@
 [dependencies.serde_yaml]
 version = "0.9.34"
 
+[dependencies.serial_test]
+version = "2"
+
 [dependencies.shlex]
 version = "1.3.0"
 
--- a/src/session/plain.rs
+++ b/src/session/plain.rs
@@ -216,6 +216,7 @@
 #[cfg(test)]
 mod tests {
     use super::*;
+    use serial_test::serial;
 
     #[test]
     fn test_prepend_user() {
@@ -360,6 +361,7 @@
 
     #[cfg(feature = "breezy")]
     #[test]
+    #[serial]
     fn test_project_from_vcs() {
         use breezyshim::tree::MutableTree;
         let env = breezyshim::testing::TestEnv::new();
--- a/src/debian/fix_build.rs
+++ b/src/debian/fix_build.rs
@@ -203,6 +203,7 @@
 #[cfg(test)]
 mod tests {
     use super::*;
+    use serial_test::serial;
     mod test_resolve_error {
         use super::*;
         use crate::debian::apt::AptManager;
@@ -316,6 +317,7 @@
         }
 
         #[test]
+        #[serial]
         fn test_missing_command_brz() {
             let env = breezyshim::testing::TestEnv::new();
             let td = tempfile::tempdir().unwrap();
