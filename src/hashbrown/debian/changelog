rust-hashbrown (0.14.5-5) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.5 from crates.io using debcargo 2.6.1
  * Really bump ahash dependency.
  * Add breaks on old version of librust-serde-with-dev.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 23 May 2024 18:02:57 +0000

rust-hashbrown (0.14.5-4) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.5 from crates.io using debcargo 2.6.1
  * Bump ahash dependency to avoid version with broken autopkgtest.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 21 May 2024 14:20:56 +0000

rust-hashbrown (0.14.5-3) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.5 from crates.io using debcargo 2.6.1
  * Remove broken attempt to fix running tests without ahash feature.
    (Closes: #1071270)

 -- Peter Michael Green <plugwash@debian.org>  Fri, 17 May 2024 20:47:33 +0000

rust-hashbrown (0.14.5-2) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.5 from crates.io using debcargo 2.6.1
  * Fix autopkgtest.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 17 May 2024 06:06:15 +0000

rust-hashbrown (0.14.5-1) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.5 from crates.io using debcargo 2.6.1
  * Upload to unstable (Closes: #1042746, #1055092)

 -- Peter Michael Green <plugwash@debian.org>  Thu, 16 May 2024 03:13:24 +0000

rust-hashbrown (0.14.3-1) experimental; urgency=medium

  * Team upload.
  * Package hashbrown 0.14.3 from crates.io using debcargo 2.6.1
  * Disable nightly features.
  * Disable rkyv feature due to dependency cycle.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 06 Feb 2024 20:09:22 +0000

rust-hashbrown (0.12.3-1) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.12.3 from crates.io using debcargo 2.5.0

 -- Blair Noctis <n@sail.ng>  Thu, 13 Oct 2022 22:10:16 -0400

rust-hashbrown (0.12.1-1) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.12.1 from crates.io using debcargo 2.5.0
  * drop patches already upstream.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 27 Jun 2022 17:02:36 -0400

rust-hashbrown (0.11.2-3) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.11.2 from crates.io using debcargo 2.5.0
  * Reinstate ahash feature (Closes: 987324)
  * Add patch to make testsuite work with rand 0.8.
  * Establish baseline for tests.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 29 May 2022 16:45:06 +0000

rust-hashbrown (0.11.2-2) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.11.2 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 01 Jan 2022 21:21:30 +0100

rust-hashbrown (0.11.2-1) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.11.2 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 01 Jan 2022 21:11:11 +0100

rust-hashbrown (0.9.1-1) unstable; urgency=medium

  * Team upload.
  * Package hashbrown 0.9.1 from crates.io using debcargo 2.4.4-alpha.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 22 Dec 2020 16:57:52 +0100

rust-hashbrown (0.1.8-1) unstable; urgency=medium

  * Package hashbrown 0.1.8 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Sat, 19 Jan 2019 12:49:57 -0800
